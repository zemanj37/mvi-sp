# MVI - Semestral

## Task
Artificial neural networks have proven to be a successful tool for creating powerful chess engines. Recent research proved that convolutional neural networks powered by reinforcement learning are superior in creating the most effective heuristics. CNNs are, on the other hand, very complex models to understand and interpret. There is, however, one type of ANN that is easy to interpret and is comparable with domain knowledge or with handcrafted heuristics. This type of ANN is logistical regression - a neural network with a single neuron.

The goal of this research is train logistical regression model by supervised and reinforcement learning and interpret its weights.

## How to run

### Supervised learning
- download data via script data/download_data.py
- process the dataset via dataset_transformer_polars.py
- train logistical regression model via train_logistical_regression.py
- analyse via logistical_regression_analysis.ipynb

### Reinforcement learning
- rfl_init.py
- rfl_initial_dataset_gen.py
- rfl.py
- analyse via logistical_regression_analysis_rfl.ipynb

Experiments are easily reproducible with common dependecies, but are expected to run multiple days.
