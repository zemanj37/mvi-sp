# Milestone
This is the first milestone for semestral project for the MVI course.

## Introduction
Artificial neural networks have proven to be a successful tool for creating powerful chess engines. Recent research proved that convolutional neural networks powered by reinforcement learning are superior in creating the most effective heuristics. CNNs are, on the other hand, very complex models to understand and interpret. There is, however, one type of ANN that is easy to interpret and is comparable with domain knowledge or with handcrafted heuristics. This type of ANN is logistical regression - a neural network with a single neuron.

## Task
The goal of this research is to understand the benefits of convolutional neural networks and reinforcement learning by utilizing simple logistical regression architecture due to its simple interpretability.

Specifically, train a logistical regression model both using supervised and reinforcement learning and compare the resulting models with each other and with a handcrafted heuristic. Then describe the limitations of such an approach and decode, where CNNs are superior.

## Survey
The game of chess is one of the most studied games in the world of AI and computer science in general. Current state-of-the-art engines utilize machine learning fundamentals as well as deep reinforcement learning to achieve superior performance. There are a few major projects that are necessary to understand before diving into the topic of this research. These projects are DeepMind's AlphaGo [1], AlphaGoZero [2] and AlphaZero [3] because they all utilize deep reinforcement learning. The open-source implementation of AlphaZero called Leela Chess is also worth mentioning. The next open-source project utilizing CNNs, which is also the strongest chess engine up to date, is Stockfish. The last major project in the chess AI domain is Maia chess since it represents a supervised learning approach. I am already familiar with all of these projects because they laid the foundation for my bachelor's thesis.

Although these projects represent the current state-of-the-art in the chess AI domain, the carryover to my research is limited, because they all utilize CNNs, which are hard to interpret. In this research, I seek something more simple, because I need to interpret pieces and positional weights. In this area, the research is still limited. There are many attempts to estimate the piece values via genetic algorithms and these attempts yield reasonable results. Although these approaches yield reasonable results, they are not very similar to my project and creating another genetic algorithm solution would be repeating work that has been already done by others. One example of such an approach is described in [this paper](https://arxiv.org/pdf/1711.08337.pdf). I was also able to find a [paper](https://cs229.stanford.edu/proj2012/DeSa-ClassifyingChessPositions.pdf) describing a supervised learning approach to estimate the piece values. However, using a reinforcement learning approach is still an open question at the end of this paper and I was not still able to find any research in this area. In the experimental part, I have already encountered some limitations of this approach, which I will describe in the report. These limitations may be the reason why I was not able to find any research in this area.

## Current state

### Dataset
The supervised model learns from the Maia-Chess dataset that is publicly available. This dataset is a pre-processed version of the publicly available Li-Chess dataset of human games in a format that can be more easily utilized for machine learning compared to the original one.

### Results
Note that this part of the research is still in progress. Firstly, the results for both supervised and reinforcement learning were obtained only by training on a small number of samples, because I did not get access to requisite computational resources, thus all models were trained in home conditions. Later subsections describe the obtained results and some first observations. However, it is expected that the results will be improved in the future.

#### Supervised learning

The supervised model weights for the pawn:

![pawn_supervised](imgs/pawn_supervised.png)

Firstly, a higher pawn value on the 7th rank is aligned with expectations, because these pawns can be promoted to the queen in just single move. Surprisingly, pawn values are not any higher in the center, a possible explanation is that pushing pawns to the center is a common move that can always be played and does not correlate with the winning probability.

When creating an optimal heuristic, on the other hand, pushing pawns to the center indeed represents a strategical advantage. The handcrafted heuristics utilizing domain knowledge would use higher value in the center. One of the goals is to compare reinforcement heuristics with supervised and with handcrafted one. The initial hypothesis is that the reinforcement learning process allows the model to find optimal heuristics to maximize the performance of the model.

The supervised model weights for the knight:

![knight_supervised](imgs/knight_supervised.png)

From the model weights for the knight, it can be seen that the corner values are highly reduced, probably due to limited mobility.

The supervised model weights for the king:

![king supervised](imgs/king_supervised.png)

From the model weights for the king, it can be seen that the corner values are highly reduced. Probably because the king often gets mated in the corners. On the other hand, this heuristic would not be optimal for the evaluation of the game tree. Such a heuristic would force the king to go into the center of the board, even though the risk of being mated can be revealed even without any heuristics. CNNs, on the other hand, would probably be able to determine the game stage (opening, middle game or endgame), which would probably result in much smarter behavior of an engine, because in the endgame it often makes sense to move the king into the center.

#### Reinforcement learning results
The average weight for the pieces taken from the reinforcement model normalized to the home rank pawn.

![pieces_values_rfl](imgs/pieces_values_rfl.png)

There are multiple observations from these weights. Firstly, the model trained by reinforcement learning values bishop slightly higher than knight, which answers the widely asked question - if the bishop has a higher value than knight.

Another observation is that pawns have on average much higher value over the board than on the first rank.

![knight_rfl](imgs/knight_rfl.png)

More interesting are the obtained values for the knight. On average, these values would provide a usable heuristic. However, we can see huge differences between the squares. Logistical regression is such a simple model that if the dataset is not fully representative (multiple games where each square is equally occupied by each piece), a value that minimizes the loss can be negative. For instance, if there are two games in the dataset where the player on turn lost with queen on H7 and there are no winning games with queen on H7, the model converges to a negative value for that specific weight.

Such a simple model requires a fully representative dataset in each step to train. CNNs, on the other hand, can easily recognize similarities in these positions, which makes them superior for the reinforcement learning process.

This behavior is not such a problem for supervised learning, where values shifted towards some unrealistic values will be self-corrected by training in other parts of the dataset. However, in the reinforcement learning process, the engine plays with these negative weights, which corrupts future data and slows down the training process.

There are some general guidelines concerning relation to the complexity of a model and the complexity of the training task. However, these guidelines typically cover supervised learning. One of the possible areas of this research is to study this connection in relation to the reinforcement learning process and describe the underlying mechanics responsible for the model's decision-making process.

## Future work
The results for supervised learning look reasonable even though the model was trained only on a small number of samples, which is expected because supervised learning is generally a much simpler task than reinforcement learning. The results for reinforcement learning, on the other hand, still exhibit a lot of randomness. I have also revealed some difficulties with reinforcement learning of such a simple model. However, although the results are not optimal yet, this research looks very promising so far, the topic is very interesting even for people who are not interested in machine learning but are interested in chess. Supervised learning with such a simple interpretable model opens up the possibility of studying human behavior in chess. By providing more features like elo, number of moves, time left, etc. it would be possible to study human behavior and adjust the strategies of human players. Comparing these results to reinforcement learning results has the potential to reveal some interesting insights about the benefits of reinforcement learning. Overall, I believe that this research has a lot of potential for an interesting blog paper, maybe even for a conference paper for the summer research project in case I will be able to surpass the reinforcement learning difficulties and find a supervisor with time capability and interest in this topic.

## References

[1] Silver, David, Aja Huang, Chris J. Maddison, Arthur Guez, Laurent Sifre, George Van Den Driessche, Julian Schrittwieser, Ioannis Antonoglou, Veda Panneershelvam, Marc Lanctot, et al. "Mastering the game of Go with deep neural networks and tree search." Nature 529.7587 (2016): 484-489.

[2] Silver, David, Julian Schrittwieser, Karen Simonyan, Ioannis Antonoglou, Aja Huang, Arthur Guez, Thomas Hubert, Lucas Baker, Matthew Lai, Adrian Bolton, et al. "Mastering the game of go without human knowledge." Nature 550.7676 (2017): 354-359.

[3] Silver, David, Thomas Hubert, Julian Schrittwieser, Ioannis Antonoglou, Matthew Lai, Arthur Guez, Marc Lanctot, Laurent Sifre, Dharshan Kumaran, Thore Graepel, et al. "A general reinforcement learning algorithm that masters chess, shogi, and Go through self-play." Science 362.6419 (2018): 1140-1144.


