"""module for model evaluations"""
import pickle
import math
from data_transform import one_hot_board

def make_evaluate_board(model_path):
    """
    creates the evaluate_board function with the model "closed" in its scope
    """
    with open(model_path, 'rb') as file:
        model = pickle.load(file)
    def evaluate_board(board):
        """
        evaluates the board for the current player
        """
        # check if the game is over
        if board.is_game_over():
            if board.is_checkmate():
                return -math.inf
            return 0
        board = one_hot_board(board)
        score = model.predict(board.reshape(1, -1))
        return score
    return evaluate_board
