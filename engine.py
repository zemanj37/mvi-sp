"""implements class for the chess engine"""
import os
import math
import multiprocessing
import random
import chess
import stockfish
from data_transform import one_hot_board


class Engine:
    """class for the chess engine"""
    def __init__(self, model):
        self.model = model
        self.randomize = True


    def evaluate_board(self, board):
        """
        evaluates the board for the current player
        """
        # check if the game is over
        if board.is_game_over():
            if board.is_checkmate():
                return -math.inf
            return 0
        board = one_hot_board(board)
        score = self.model.predict_proba(board.reshape(1, -1))[0][1]
        # print(self.model.classes_)
        # print(score)
        # if result is 0-1
        score -= 0.5
        score *=2
        return score


    def __evaluate_wrapper(self, board, player_color):
        """
        evaluates the board for a specific player
        all his pieces are positive and all the opponent's pieces are negative
        """
        score = self.evaluate_board(board)
        if player_color != board.turn:
            score *= -1
        return score


    @staticmethod
    def move_is_check_or_mate(board, move):
        """
        checks if a move gives check
        """
        board.push(move)
        result = board.is_check() or board.is_checkmate()
        board.pop()
        return result


    def __alphabeta_minimax(self, board, depth, player_color, alpha=-math.inf, beta=math.inf):
        """
        Minimax algorithm with alpha-beta pruning
        """
        if depth == 0 or board.is_game_over():
            return self.__evaluate_wrapper(board, player_color)

        # sort the moves, co capture moves first or moves that give check first
        sorted_moves = sorted(board.legal_moves, key=lambda move: board.is_capture(move) or Engine.move_is_check_or_mate(board, move), reverse=True)
        if board.turn == player_color:
            best_score = -math.inf
            for move in sorted_moves:
                board.push(move)
                score = self.__alphabeta_minimax(board, depth-1, player_color, alpha, beta)
                board.pop()
                best_score = max(best_score, score)
                alpha = max(alpha, best_score)
                if beta <= alpha:
                    break  # beta cutoff
            return best_score
        best_score = math.inf
        for move in sorted_moves:
            board.push(move)
            score = self.__alphabeta_minimax(board, depth-1, player_color, alpha, beta)
            board.pop()
            best_score = min(best_score, score)
            beta = min(beta, best_score)
            if beta <= alpha:
                break  # alpha cutoff
        return best_score


    def evaluate_move(self, move, board, depth, maximizing_player_color):
        """gets a move and returns the score of the move"""
        board.push(move)
        score = self.__alphabeta_minimax(board, depth - 1, maximizing_player_color)
        board.pop()
        return (move, score)


    def list_of_best_moves(self, board, depth):
        """returns a list of the best moves"""
        maximizing_player_color = board.turn

        MULTIPROCESSING = False
        if MULTIPROCESSING:
            with multiprocessing.Pool() as pool:
                move_scores = pool.starmap(self.evaluate_move, [(move, board.copy(), depth, maximizing_player_color) for move in board.legal_moves])
        else:
            move_scores = [self.evaluate_move(move, board.copy(), depth, maximizing_player_color) for move in board.legal_moves]

        # best score
        _, best_score = max(move_scores, key=lambda x: x[1])
        if self.randomize:
            best_score -= 0.02
        # list of moves with the best score
        best_moves = [move for move, score in move_scores if score >= best_score]
        return best_moves


    def select_best_move(self, board, depth):
        """
        Selects the best move using the minimax algorithm
        """
        final_list = self.list_of_best_moves(board, depth)
        if len(final_list) == 1 or depth == 2:
            return random.choice(final_list)

        best_moves_in_depths = []
        for i in range(2, depth, 2):
            best_moves_in_depths.append(self.list_of_best_moves(board, i))

        # flatten the list
        shallow_depths = [move for sublist in best_moves_in_depths for move in sublist]
        original_best_moves = final_list.copy()
        # exclude moves that are not in any of the best moves in the depths
        final_list = [move for move in final_list if move in shallow_depths]
        # if there are no moves in the shallow depths, return random choice from original best moves
        if len(final_list) == 0:
            return random.choice(original_best_moves)
        return random.choice(final_list)


def stockfish_play_forced_mate(board, sf):
    """if stockfish found forced mate, let it play"""
    # set position to stockfish
    sf.set_fen_position(board.fen())
    # get evaluation
    stockfish_evaluation = sf.get_evaluation()
    # if evaluation is mate and I am winning, return stockfish move
    if stockfish_evaluation["type"] == "mate":
        # return stockfish move only if I am winning
        if (stockfish_evaluation["value"] > 0 and board.turn == chess.WHITE) or (stockfish_evaluation["value"] < 0 and board.turn == chess.BLACK):
            move = sf.get_best_move()
            # convert move to chess.Move
            move = chess.Move.from_uci(move)
            return move
    return None




class Stockfish_wrapper():
    """takes stockfish path and uses stockfish to play move, if it found forced mate"""
    def __init__(self, stockfish_path, stockfish_depth, model):
        self.__engine = Engine(model)
        self.__stockfish = stockfish.Stockfish(stockfish_path)
        self.__stockfish.set_depth(stockfish_depth)


    def select_best_move(self, board, depth):
        """if stockfish found mate, let it play"""
        move = stockfish_play_forced_mate(board, self.__stockfish)
        if move is not None:
            return move
        return self.__engine.select_best_move(board, depth)
