"""initializes the rfl model with a random game"""
import pickle
import numpy as np
import chess
from sklearn.linear_model import SGDRegressor
from data_transform import one_hot_board


# rfl model initialization
model = SGDRegressor(loss='squared_error', penalty='l2', alpha=0.1, fit_intercept=False, max_iter=100, shuffle=True, learning_rate='optimal', eta0=0.001)
board = chess.Board()
X = [one_hot_board(board)]
y = [0]
model.partial_fit(X, y)
coefs = model.coef_
# input random values
coefs = np.random.normal(0.1, 0.01, coefs.shape)

print(coefs)
model.coef_ = coefs

# save model
with open('./data/logreg_rfl_original.pickle', 'wb') as f:
    pickle.dump(model, f)
