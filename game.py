"""implements class for one game"""
import random
import chess
from engine import Stockfish_wrapper

class Game():
    """implements class for one game"""
    def __init__(self, model):
        self.engine = Stockfish_wrapper('stockfish', stockfish_depth=8, model=model)
        self.white_won = False
        self.black_won = False
        self.boards = []


    def play(self, depth=4):
        """play the game"""
        board = chess.Board()
        # push first moves randomly
        for _ in range(6):
            self.boards.append(board.copy())
            if board.is_game_over():
                break
            board.push(random.choice(list(board.legal_moves)))
        while not board.is_game_over():
            self.boards.append(board.copy())
            board.push(self.engine.select_best_move(board, depth=depth))
        self.boards.append(board.copy())
        if board.is_checkmate():
            if board.turn:
                self.black_won = True
            else:
                self.white_won = True
