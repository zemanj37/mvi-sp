"""module responsible for training logistical regression model using classification method"""
import os
import random
import pickle
import pandas as pd
from sklearn.linear_model import SGDRegressor, SGDClassifier
from sklearn.neural_network import MLPRegressor, MLPClassifier

import warnings
warnings.filterwarnings('ignore')

# create logistic regression model without regularization
# logreg = SGDRegressor(loss='squared_error', penalty='l2', alpha=0, fit_intercept=False, max_iter=100, shuffle=True, learning_rate='constant', eta0=0.000001)
model = MLPRegressor(hidden_layer_sizes=(1,), activation='tanh', max_iter=1000)
# create logistic regression model with small amount of regularization
# logreg = SGDRegressor(loss='squared_error', penalty='l2', alpha=0.000001, fit_intercept=False, max_iter=100, shuffle=True, learning_rate='optimal')


def count_files():
    """count number of training files in data directory"""
    count = 0
    for filename in os.listdir("./data"):
        if filename.startswith("transformed_") and filename.endswith(".parquet"):
            count += 1
    return count

# df_transformed = pd.read_parquet('./data/maia-chess-testing-set_{}.parquet')
base_filename = './data/transformed_{}.parquet'
num_files = count_files()
print("Number of files: {}".format(num_files))
filenames = [base_filename.format(i) for i in range(num_files)]

for i in range(8):
    # random shuffle list of filenames
    random.shuffle(filenames)
    for j, filename in enumerate(filenames):
        print(f'{i+1}th epoch - {round(100 * j / num_files, 2)}%   \r', end='')

        # prepare training data pandas
        df_transformed = pd.read_parquet(filename, columns=['board', 'linear_win'])
        X_train = df_transformed['board'].tolist()
        y_train = df_transformed['linear_win'].tolist()

        # fit the model
        model.partial_fit(X_train, y_train)
        # Display the trained weights
        print("Trained Weights:")
        print("Input Layer Weights:", model.coefs_[0])
        print("Output Layer Weights:", model.coefs_[1])
        print("Output Layer Biases:", model.intercepts_[1])
        break

    # save model after each epoch
    with open('./data/mlp_tanh.pickle', 'wb') as f:
        pickle.dump(model, f)
print('done')
