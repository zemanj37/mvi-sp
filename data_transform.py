"""module for transforming input and output"""
import numpy as np
import chess


def square_to_index(square):
    """
    converts square to tuple of indexes
    """
    rank_index = chess.square_rank(square)
    file_index = chess.square_file(square)
    return (7 - rank_index, file_index)


def mirror_indexes_for_black(indexes):
    """
    returns indexes for black
    """
    return (7 - indexes[0], indexes[1])


def square_to_index_for_color(square, color):
    """
    converts square to tuple of indexes
    """
    if color == chess.BLACK:
        return mirror_indexes_for_black(square_to_index(square))
    return square_to_index(square)


def board_to_array(board):
    """converts board to array"""
    board3d = np.zeros((6, 8, 8), dtype=np.int8)
    first = board.turn
    second = not board.turn
    for piece in chess.PIECE_TYPES:
        for square in board.pieces(piece, first):
            board3d[piece - 1][square_to_index_for_color(square, first)] += 1
        for square in board.pieces(piece, second):
            board3d[piece - 1][square_to_index_for_color(square, second)] -= 1

    return board3d


def one_hot_board(board):
    """converts board to one hot array"""
    board3d = board_to_array(board)
    # flatten
    board1d = board3d.flatten()
    return board1d
