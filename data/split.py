import os
from itertools import islice

FILENAMES = [f'maia_chess_training_set{i}.csv' for i in range(12)]
N_LINES = 10000000

def write_chunk_to_file(chunk_number, lines, original_filename):
    output_filename = f"{original_filename.split('.')[0]}_{chunk_number}.csv"
    with open(output_filename, 'w') as output_file:
        output_file.writelines(lines)

def split_large_csv(original_filename):
    with open(original_filename, 'r') as input_file:
        # Read and write header
        header = input_file.readline()
        for i, chunk in enumerate(iter(lambda: list(islice(input_file, N_LINES)), [])):
            write_chunk_to_file(i, [header] + chunk, original_filename)
            print(f"File {original_filename.split('.')[0]}_{i}.csv created.")

for i, filename in enumerate(FILENAMES):
    split_large_csv(filename)
    os.remove(filename)
    print(f"Original file {filename} removed.")
    print(f"processed {i+1}/{len(FILENAMES)} files.")
