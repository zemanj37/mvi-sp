"""module to download and unpack data"""
import os
import urllib.request
import bz2


def unpack(source_filename, dest_filename):
    """unpacks bz2 file"""
    with open(source_filename, 'rb') as source, open(dest_filename, 'wb') as target:
        decompressor = bz2.BZ2Decompressor()
        for data in iter(lambda: source.read(100 * 1024), b''):
            target.write(decompressor.decompress(data))


URLS = ['http://csslab.cs.toronto.edu/data/chess/kdd/maia-chess-testing-set.csv.bz2']
URLS += [f'http://csslab.cs.toronto.edu/data/chess/monthly/lichess_db_standard_rated_2019-{i:02d}.csv.bz2' for i in range(1, 13)]
FILENAMES = ['maia_chess_testing_set.csv.bz2']
FILENAMES += [f'maia_chess_training_set{i}.csv.bz2' for i in range(12)]

print(URLS)
print(FILENAMES)

for url, filename in zip(URLS, FILENAMES):
    print(f"Downloading {filename}")
    urllib.request.urlretrieve(url, filename)
    print(f"Downloaded {filename}")

    unpacked_file_name = filename[:-4]
    print(f"Unpacking {filename}")
    unpack(filename, unpacked_file_name)
    print(f"Unpacked {filename}")
    # remove bz2 file
    os.remove(filename)
