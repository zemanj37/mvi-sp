from multiprocessing import Pool
from tqdm import tqdm
import pickle
import numpy as np
import chess
from game import Game
from data_transform import one_hot_board


def reward(board, white_won : bool, black_won : bool):
    """return reward for the current board"""
    # draw
    if not white_won and not black_won:
        return 0
    # player on turn won
    if (white_won and board.turn == chess.WHITE) or (black_won and board.turn == chess.BLACK):
        return 1
    # player on turn lost
    return -1


def worker(model_path : str):
    """worker function for training, loads the model, plays it and returns the game"""
    with open(model_path, 'rb') as f:
        model = pickle.load(f)
    game = Game(model)
    game.play(2)
    return game.boards, game.white_won, game.black_won


def normalize_model(model):
    """takes trained model, all negative coefficients are set to small positive values"""
    coefs = model.coef_
    coefs_positive = np.random.normal(0.001, 0.0001, coefs.shape)
    result_coef = np.where(coefs < 0, coefs_positive, coefs)
    model.coef_ = result_coef
    return model


LOGREG_FILENAME = './data/logreg_rfl_original.pickle'
with open(LOGREG_FILENAME, 'rb') as f:
    model = pickle.load(f)

RAW_START = True
if not RAW_START:
    # do initial fit
    N_FILES = 5
    X = []
    y = []
    for i in range(100 - N_FILES, 100):
        with open(f'./data/dataset_rfl_{i}.pickle', 'rb') as f:
            data = pickle.load(f)
        X.extend(data[0])
        y.extend(data[1])

    # print len of data
    print(f'len(data) = {len(X)}')
    model.partial_fit(X, y)

    # normalize model
    model = normalize_model(model)
    
    # save model
    with open('./data/logreg_rfl.pickle', 'wb') as f:
        pickle.dump(model, f)


for i in range(100):
    print(f'iteration {i}')
    games = [LOGREG_FILENAME for _ in range(1000)]
    with Pool() as pool:
        results = []
        for result in tqdm(pool.imap(worker, games), total=len(games)):
            results.append(result)
    
    X = []
    y = []
    print('processing results')
    for boards, white_won, black_won in results:
        # if draw, continue
        if not white_won and not black_won:
            continue
        # display(boards[-1])

        # prepare rewards
        rewards = [reward(board, white_won, black_won) for board in boards]
        # skew rewards linearly
        rewards = [r * (i / len(rewards)) for i, r in enumerate(rewards)]

        # prepare boards
        boards = [one_hot_board(board) for board in boards]
        # print stats
        # print(f'len of the game: {len(boards)}')
        # print(rewards)
        X.extend(boards)
        y.extend(rewards)
    
    # save dataset
    print(f'len of dataset {len(X)}')
    with open(f'./data/dataset_rfl_initial_{i}.pickle', 'wb') as f:
        pickle.dump((X, y), f)
    
    # train model
    # print('training model')
    # model.partial_fit(X, y)
    
    # save model
    with open('./data/logreg_rfl.pickle', 'wb') as f:
        pickle.dump(model, f)
print('done')
