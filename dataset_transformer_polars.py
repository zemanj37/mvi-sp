"""module fot processing dataset using polars"""
import os
import random
import multiprocessing as mp
import glob
from tqdm import tqdm
import polars as pl
import chess
import data_transform


FILENAMES = glob.glob('./data/maia_chess_training_set*_*.csv')
# FILENAMES = ['data/maia_chess_testing_set.csv']
print(f'about to process {FILENAMES,}')


NESESSARY_COLUMNS = ['board', 'move', 'white_won', 'black_won', 'move_ply', 'game_id', 'no_winner']


def transform_game_df(game_df):
    """gets a polars df and returns transformed polars df"""

    # add new column player_on_turn_won
    game_df = game_df.with_columns(
        pl.when(
            (game_df['white_won'] & (game_df['move_ply'] % 2 == 0)) |
            (game_df['black_won'] & (game_df['move_ply'] % 2 == 1))
        ).then(True).otherwise(False).alias('player_on_turn_won')
    )

    # add column linear_win
    game_df = game_df.with_columns(
        pl.when(
            (game_df['player_on_turn_won'])
        ).then(1).otherwise(-1).alias('linear_win')
    )
    game_df = game_df.with_columns( (game_df['linear_win'] * (~game_df['no_winner'] * (game_df['move_ply'] / len(game_df)))).alias('linear_win') )


    # keep only necessary columns
    game_df = game_df[['board', 'move', 'player_on_turn_won', 'linear_win']]
    return game_df


def worker(fen):
    """function to be used in parallel processing"""
    return data_transform.one_hot_board(chess.Board(fen))


def transform_boards(game_df):
    """gets a polars df and returns transformed polars df"""

    # Apply the one-hot encoding function to each board in the "board" column using multiple threads
    board_list = game_df['board'].to_list()
    with mp.Pool(mp.cpu_count()) as pool:
        boards_transformed = pool.map(worker, board_list)
    # Convert the list of one-hot encoded boards to a polars Series
    one_hot_boards = pl.Series(boards_transformed)

    # Remove the original "board" column and add the new one-hot encoded column with the same name
    game_df = game_df.drop('board').select(pl.col("*"), one_hot_boards.alias("board"))

    return game_df


def count_files():
    """counts the number of files in the data folder"""
    count = 0
    for filename in os.listdir("./data"):
        if filename.startswith("transformed_") and filename.endswith(".parquet"):
            count += 1
    return count


def transform_csv_file(filename):
    """transforms a csv file and saves it as multiple parquet files"""
    print(f"Transforming {filename}")
    df = pl.read_csv(filename, columns=NESESSARY_COLUMNS)
    n_rows = df.shape[0]
    print(f"Number of rows: {n_rows}")

    # split polars dataframe into multiple dataframes using column "game_id"
    games = list(df.group_by('game_id'))
    n_games = len(games)
    print(f'number of games: {n_games}')

    n_games_in_batch = 1000000 // (n_rows / n_games)
    print(f'number of games in batch: {n_games_in_batch}\n')

    transformed_dfs = []
    n_datasets_written = count_files()
    # for i, (game_name, game_df) in enumerate(games):
    while games:
        # print progress
        if not random.randint(0, 100):
            print(f'{round((n_games - len(games)) * 100 / n_games, 2)}% games processed, still remaining: {len(games)}\r', end='')

        # select game and remove from the list
        current_game = games[0][1]
        games.pop(0)

        # transform game
        current_game = transform_game_df(current_game)
        transformed_dfs.append(current_game)

        # save batch if it is big enough or if there are no more games
        if len(transformed_dfs) >= n_games_in_batch or not games:
            # concatenate all transformed dataframes into one dataframe
            transformed_df = pl.concat(transformed_dfs)
            transformed_df = transform_boards(transformed_df)
            # save as parquet file
            transformed_df.write_parquet(f'./data/transformed_{n_datasets_written}.parquet')
            n_datasets_written += 1
            transformed_dfs.clear()

    print('100% /- finished\n')

for filename in tqdm(FILENAMES):
    transform_csv_file(filename)
    # remove csv file
    os.remove(filename)
